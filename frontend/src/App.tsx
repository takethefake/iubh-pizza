import * as React from "react";
import "./App.css";
import { WarenkorbProvider } from "./provider/WarenkorbProvider";
import { PizzaProvider } from "./provider/PizzaProvider";
import { PizzaPage } from "./pages/PizzaPage";
import { Link, Switch, Route, BrowserRouter, Redirect } from "react-router-dom";
import { OrderPage } from "./pages/OrderPage";
import { urls } from "./utils/urls";

function App() {
  return (
    <PizzaProvider>
      <WarenkorbProvider>
        <BrowserRouter>
          <div className="App">
            <header className="App-header">
              <h1>Pizza Napoli</h1>
              <nav>
                <ul>
                  <li>
                    <Link to={urls.speisekarte}>Speisekarte</Link>
                  </li>
                  <li>
                    <Link to={urls.bestellungen}>Bestellungen</Link>
                  </li>
                </ul>
              </nav>
            </header>
            <Switch>
              <Route path={urls.speisekarte}>
                <PizzaPage />
              </Route>
              <Route path={urls.bestellungen}>
                <OrderPage />
              </Route>
              <Route>
                <Redirect to={urls.speisekarte} />
              </Route>
            </Switch>
          </div>
        </BrowserRouter>
      </WarenkorbProvider>
    </PizzaProvider>
  );
}

export default App;
