import styled from "@emotion/styled";
import { useWarenkorb } from "../provider/WarenkorbProvider";
import { usePizza } from "../provider/PizzaProvider";

export const WarenkorbWrapper = styled.div`
  flex: 1;
  height: 100vh;
  background: #eeeeee;
`;

export const Warenkorb = () => {
  const { pizzaById } = usePizza();
  const {
    items,
    actions: { orderWarenkorb },
  } = useWarenkorb();

  console.log("warenkorb", items);
  return (
    <WarenkorbWrapper>
      {Object.entries(items).map(([pizzaId, pizzaCount]) => {
        return (
          <div>
            {pizzaById[pizzaId]?.name}
            <p>{pizzaCount}</p>
          </div>
        );
      })}
      <button
        onClick={orderWarenkorb}
        disabled={Object.keys(items).length === 0}
      >
        Abschicken
      </button>
    </WarenkorbWrapper>
  );
};
