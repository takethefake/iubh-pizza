import { Bestellung } from "../types";
import { formatDateTime } from "../utils/date";

type OrderItem = {
  order: Bestellung;
};

export const OrderItem = ({ order }: OrderItem) => {
  return (
    <div>
      {formatDateTime(order.createdDate)}
      <ul>
        {order.pizzaBestellungen.map((bestellung) => (
          <li>
            {bestellung.anzahl} *{bestellung.pizza.name}
          </li>
        ))}
      </ul>
    </div>
  );
};
