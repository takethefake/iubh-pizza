import styled from "@emotion/styled";
import { useWarenkorb } from "../provider/WarenkorbProvider";
import { Pizza } from "../types";

type PizzaItem = {
  pizza: Pizza;
};

export const PizzaItemContainer = styled.div`
  border: 1px #ccc;
  width: 500px;
  padding: 16px;
  display: flex;
  flex-direction: column;
`;

export const PizzaItem = ({ pizza }: PizzaItem) => {
  const { description, name } = pizza;
  const {
    actions: { addPizzaToWarenkorb },
  } = useWarenkorb();

  return (
    <PizzaItemContainer>
      <h2>{name}</h2>
      <p>{description}</p>
      <button
        onClick={() => {
          addPizzaToWarenkorb(pizza);
        }}
      >
        +
      </button>
    </PizzaItemContainer>
  );
};
