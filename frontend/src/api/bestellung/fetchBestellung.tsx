import parseIso from "date-fns/parseISO";
import { Bestellung, BestellungResponse } from "../../types";

const transformOrder = (responseData: BestellungResponse): Bestellung => ({
  ...responseData,
  createdDate: parseIso(responseData.createdDate),
});

type BestellungRequestOptions = {};
export const fetchBestellung = async (
  options?: BestellungRequestOptions
): Promise<Bestellung[]> => {
  return await fetch("/api/bestellung", options)
    .then((res) => res.json())
    .then((jsonData) => jsonData.map(transformOrder));
};
