import format from "date-fns/format";

export const formatTime = (date: Date) => {
  return format(date, "HH:mm");
};
export const formatDate = (date: Date) => {
  format(date, "dd.MM.yyyy");
};
export const formatDateTime = (date: Date) => {
  return format(date, "dd.MM.yyyy HH:mm");
};
