import { PizzaItem } from "../components/PizzaItem";
import { Warenkorb } from "../components/Warenkorb";
import * as React from "react";
import { usePizza } from "../provider/PizzaProvider";

export const PizzaPage = () => {
  const { pizzen } = usePizza();
  return (
    <div style={{ display: "flex" }}>
      <div style={{ flex: "2" }}>
        {pizzen.map((pizza) => {
          return <PizzaItem pizza={pizza} />;
        })}
      </div>
      <Warenkorb />
    </div>
  );
};
