import { Bestellung } from "../types";
import React from "react";
import { OrderItem } from "../components/OrderItem";
import { fetchBestellung } from "../api/bestellung/fetchBestellung";

export const OrderPage = () => {
  const [orders, setOrders] = React.useState<Bestellung[]>([]);
  const fetchOrder = async () => {
    const bestellungen = await fetchBestellung();
    setOrders(bestellungen);
  };

  React.useEffect(() => {
    fetchOrder();
  }, []);

  return (
    <>
      <h1>Orders</h1>
      <div>
        {orders.map((order) => (
          <OrderItem order={order} />
        ))}
      </div>
    </>
  );
};
