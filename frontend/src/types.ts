export interface Bestellung {
  id: string;
  kundennummer: string;
  createdDate: Date;
  pizzaBestellungen: PizzaBestellungen[];
}

export interface BestellungResponse {
  id: string;
  kundennummer: string;
  createdDate: string;
  pizzaBestellungen: PizzaBestellungen[];
}

export interface PizzaBestellungen {
  anzahl: number;
  pizza: Pizza;
}

export interface Pizza {
  id: string;
  name: string;
  description: string;
}
