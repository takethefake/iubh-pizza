import { useCallback, useState } from "react";

export function useLocalStorage<Value>(key: string, initialValue: Value) {
  return useStorage<Value>(key, initialValue, window.localStorage);
}

export function useSessionStorage<Value>(key: string, initialValue: Value) {
  return useStorage<Value>(key, initialValue, window.sessionStorage);
}

function useStorage<Value>(key: string, initialValue: Value, storage: Storage) {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = useState(() => {
    try {
      // Get from local storage by key
      const item = storage.getItem(key);
      // Parse stored json or if none return initialValue
      return item ? parseJSON(item) : initialValue;
    } catch (error) {
      // If error also return initialValue
      // eslint-disable-next-line no-console
      console.error(error);
      return initialValue;
    }
  });

  // Return a wrapped version of useState's setter function that ...
  // ... persists the new value to localStorage.
  const setValue = useCallback(
    (value: Value | ((prev: Value) => Value)) => {
      try {
        // Allow value to be a function so we have same API as useState
        const valueToStore =
          typeof value === "function" ? (value as any)(storedValue) : value;
        // Save state
        setStoredValue(valueToStore);
        // Save to local storage
        storage.setItem(key, JSON.stringify(valueToStore));
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    },
    [key, storedValue, storage]
  );

  return [storedValue, setValue] as const;
}

export function parseJSON(value: unknown) {
  try {
    if (typeof value !== "string") {
      return null;
    }

    return JSON.parse(value);
  } catch {
    return null;
  }
}
