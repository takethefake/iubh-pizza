import * as React from "react";
import { Pizza } from "../types";
import { useLocalStorage, useSessionStorage } from "../hooks/useStorage";

type WarnkorbContext = {
  items: Record<string, number>; // {"idFürSalamiPizza":2}
  actions: {
    addPizzaToWarenkorb: (pizza: Pizza) => void;
    orderWarenkorb: () => void;
  };
};

const warenkorbContext = React.createContext<WarnkorbContext>({
  items: {},
  actions: { addPizzaToWarenkorb: () => {}, orderWarenkorb: () => {} },
});

export const WarenkorbProvider: React.FC = ({ children }) => {
  const [items, setItems] = useSessionStorage<Record<string, number>>(
    "warenkorb-items",
    {}
  );
  const addPizzaToWarenkorb = (pizza: Pizza) => {
    setItems((prevItems) => ({
      ...prevItems,
      [pizza.id]: prevItems?.[pizza.id] ? prevItems[pizza.id] + 1 : 1,
    }));
  };

  const orderWarenkorb = async () => {
    const res = await fetch("/api/bestellung", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({
        kundennummer: "1234",
        items: items
          ? Object.entries(items).map(([pizzaId, pizzaAnzahl]) => ({
              id: pizzaId,
              anzahl: pizzaAnzahl,
            }))
          : [],
      }),
    });
    if (res.status === 200) {
      setItems({});
      alert("Bestellung wurde abgeschickt");
    }
  };
  return (
    <warenkorbContext.Provider
      value={{
        items: items ?? {},
        actions: { addPizzaToWarenkorb, orderWarenkorb },
      }}
    >
      {children}
    </warenkorbContext.Provider>
  );
};

export function useWarenkorb() {
  return React.useContext(warenkorbContext);
}
