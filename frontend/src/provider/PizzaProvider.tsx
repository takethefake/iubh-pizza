import * as React from "react";
import { Pizza } from "../types";

type PizzaContext = {
  pizzen: Pizza[];
  pizzaById: Record<string, Pizza>;
};

const pizzaContext = React.createContext<PizzaContext>({
  pizzen: [],
  pizzaById: {},
});

export const PizzaProvider: React.FC<{}> = ({ children }) => {
  const [pizzen, setPizzen] = React.useState<Pizza[]>([]);
  const fetchData = async function () {
    // body of the function
    const data = await fetch("/api/pizza").then((data) => data.json());
    setPizzen(data);
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  ///  [{id:"2",name:'test'},{id:"3",name:'test3'},....]
  // {"2":{id:"2",name:'test',....}}

  // reduce nimmt sich pro runde ein element vom array `{id:"2",name:'test'} `

  const pizzaById = pizzen.reduce<Record<string, Pizza>>((prev, curPizza) => {
    prev[curPizza.id] = curPizza;
    return prev;
  }, {});

  return (
    <pizzaContext.Provider value={{ pizzaById, pizzen }}>
      {children}
    </pizzaContext.Provider>
  );
};

export function usePizza() {
  return React.useContext(pizzaContext);
}
