import { Router } from "express";
import { getPizza, postPizza } from "../controller/pizza.controller";

export const pizzaRouter = Router({ mergeParams: true });

pizzaRouter.get("/", getPizza);
pizzaRouter.post("/", postPizza);
