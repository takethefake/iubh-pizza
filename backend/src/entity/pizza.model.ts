import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import * as yup from "yup";
import { PizzaBestellung } from "./pizzaBestellung.model";

interface PizzaInitProps {
  name: string;
  description: string;
}

export const pizzaSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

@Entity()
export class Pizza {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  name!: string; //margarita

  @Column()
  description!: string; //margarita

  @OneToMany(() => PizzaBestellung, (pizzaBestellung) => pizzaBestellung.pizza)
  pizzaBestellungen!: PizzaBestellung[]; // id 2

  static create(props: PizzaInitProps): Pizza {
    const pizza = new Pizza();
    pizza.name = props.name;
    pizza.description = props.description;
    return pizza;
  }
}
